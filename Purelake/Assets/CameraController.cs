/* Modified from, https://learn.unity.com/tutorial/controlling-unity-camera-behaviour */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // The camera turns the target and listens for mouse input
    [SerializeField] Transform target;  // player
    [SerializeField] float turnSpeed = 300.0f;

    [Tooltip("...")]
    [SerializeField] float maxCameraHeight = 6f;
    [SerializeField] [Range(0, 6)] float cameraHeight = 3f;

    [SerializeField] float cameraScrollSpeed = 300.0f;
    [SerializeField] float cameraMoveSpeed = 300.0f;

    [Tooltip("MeshRenderer is hidden when zoomed past this distance")]
    [SerializeField] float hideDistance = 1.5f;
    
    private MeshRenderer meshRenderer;
    private GameObject ahead;  // The camera looks toward this GameObject

    void Start()
    {
        ahead = new GameObject("ahead");
        meshRenderer = target.gameObject.GetComponent<MeshRenderer>();
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(1))  // Rotate when right mouse button is held down
        {
            // The camera rotates the player transform
            var axis1 = Input.GetAxisRaw("Mouse X") * turnSpeed * Time.deltaTime;
            target.rotation = Quaternion.Euler(0, axis1, 0) * target.rotation;
        }
    }

    void LateUpdate()
    {
        // Determine cameraHeight from input
        cameraHeight += Input.GetAxisRaw("Mouse ScrollWheel") * cameraScrollSpeed * Time.deltaTime;
        cameraHeight = Mathf.Clamp(cameraHeight, 0, maxCameraHeight);

        var newPosition = target.position + Vector3.up * cameraHeight
                           - target.forward * cameraHeight * 1.3f; // push it back a little extra

        // MoveWowards new position
        transform.position = Vector3.MoveTowards(transform.position, newPosition, 
                                                   cameraMoveSpeed * Time.deltaTime);

        // Look here
        ahead.transform.position = target.position + target.forward * (maxCameraHeight);
        transform.LookAt(ahead.transform);

        // Hide MeshRenderer if close
        meshRenderer.enabled = (cameraHeight > hideDistance);
    }
}

